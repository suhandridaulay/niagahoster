<?php
require_once('config.php');
require_once('Database.php');
require_once('Package.php');

$connection = new Database(dbName, dbUser, dbPass, dbHost);
$package = new Package($connection);

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Niagahoster</title>
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/materialdesignicons.css" media="all" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.css" rel="stylesheet">
        <link href="assets/css/responsive.css" rel="stylesheet">
    </head>
    <body>
        <header>
            <div class="header-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 d-none d-md-block d-lg-block pt-2"><p>Gratis Ebook 9 Cara Cerdas Menggunakan Domain [ x ]</p></div>
                        <div class="col-md-7">
                            <ul class="menu_header_top">
                                <li class=""><a href="#"><i class="mdi mdi-phone"></i>0274-5305505</a></li>
                                <li class=""><a href="#"><i class="mdi mdi-message"></i> Live Chat</a></li>
                                <li><a href="#"><i class="mdi mdi-account-circle md-2x"></i>Member Area</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="menu">
                <div class="container">
                    <div class="row">
                        <div class="col-6 col-md-3 col-lg-3">
                            <a href=""><img src="assets/images/logo.png" class="img-fluid"></a>
                        </div>
                        <div class="col-6 col-md-9 col-lg-9">
                            
                            <nav id="navbar" class="collapse navbar-collapse">
                                <ul id="nav">
                                    <li><a href="contact.html">Hosting</a></li>
                                    <li><a href="contact.html">Domain</a></li>
                                    <li><a href="contact.html">Server</a></li>
                                    <li><a href="contact.html">Website</a></li>
                                    <li><a href="contact.html">Affiliasi</a></li>
                                    <li><a href="contact.html">Promo</a></li>
                                    <li><a href="contact.html">Pembayaran</a></li>
                                    <li><a href="contact.html">Review</a></li>
                                    <li><a href="contact.html">Kontak</a></li>
                                    <li><a href="contact.html">Blog</a></li>
                                </ul>
                            </nav>
                            <div class="navbar-header text-right">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <span class="sr-only"></span>
                                    <i class="mdi mdi-menu"></i>
                                </button>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </header>

        <div class="slider pt-5 pb-4">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                        <h2 class="fontmontserrat font-weight-bold">PHP Hosting</h2>
                        <h3 class="fontmontserrat">Cepat, handal, penuh dengan modul PHP yang Anda butuhkan</h3>
                        <ul class="feature-slider">
                            <li><i class="mdi mdi-check-circle"></i> Solusi PHP untuk performa query yang lebih cepat.</li>
                            <li><i class="mdi mdi-check-circle"></i> Konsumsi memori yang lebih rendah</li>
                            <li><i class="mdi mdi-check-circle"></i> Support PHP 5.3, PHP 5.4, PHP 5.5, PHP 5.6, PHP 7</li>
                            <li><i class="mdi mdi-check-circle"></i> Fitur enkripsi IonCube dan Zend Guard Loaders</li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 text-right">
                        <div class="homeImgTable">
                            <div class="homeImg">
                                <img src="assets/svg/illustrationbannerPHPhosting-01.svg" alt="" class="img-fluid" width="450">
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

        <div class="installservice pt-4 pb-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-4 col-xs-4 col-sm-4 col-md-3 col-lg-3 text-center colinstall">
                        <img src="assets/svg/illustration-banner-PHP-zenguard01.png" class="img-fluid">
                        <p class="mt-3">PHP Zend Guard Loader</p>
                    </div>
                    <div class="col-4 col-xs-4 col-sm-4  col-md-3 col-lg-3 text-center colinstall" >
                        <img src="assets/svg/composer.png" class="img-fluid">
                        <p class="mt-3">PHP Composer</p>
                    </div>
                    <div class="col-4 col-xs-4 col-sm-4  col-md-3 col-lg-3 text-center colinstall">
                        <img src="assets/svg/icon-php-hosting-ioncube.png" class="img-fluid">
                        <p class="mt-3">PHP IonCube Loader</p>
                    </div>
                    <div class="col-md-1"></div>
                    
                </div>
            </div>
        </div>

        <div class="package pt-4 pb-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="sectionTitle text-center">
                            <h2 class="font-weight-bold">Paket Hosting Singapura yang Tepat</h2>
                            <h3 class="mb-5">Diskon 40% + Domain dan SSL Gratis untuk Anda</h3>
                        </div>
                    </div>
                </div>
                <div class="row price clearfix">
                    <?php
                    $price = $package->getAll();
                    if($price){
                        foreach($price as $data){
                            $price = $data['packagePrice'] - $data['packageDisc'];
                    ?>
                    <div class="col-lg-3 offset-lg-0 priceCol col-md-5 offset-md-1">
                        <div class="singlePrice <?php if($data['packageStatusBest'] == 'y') echo 'active'; ?>">
                            <div class="priceTitle"><?php echo $data['packageName']; ?></div>
                            <div class="priceHead">
                                <?php
                                if($data['packageDisc'] > 0){
                                ?>
                                <div class="currencyold"><del>Rp <?php echo number_format($data['packagePrice'],'0','.','.'); ?></del></div>
                                <?php
                                }
                                ?>
                                <p><span>Rp</span> <?php echo number_format($price,'0','.','.'); ?><span class="currency_month">/bln</span></p>
                            </div>
                            <div class="priceUser"><strong><?php echo number_format($data['packageUser'],'0','.','.'); ?></strong> Pengguna Terdaftar</div>
                            <ul class="priceBody">
                                <?php
                                $explode = explode('|',$data['packageDesc']);
                                if($explode){
                                    foreach($explode as $e){
                                ?>
                                <li><?php echo $e; ?></li>
                                <?php
                                    }
                                }
                                ?>
                            </ul>
                            <button class="<?php if($data['packageStatusBest'] == 'y') echo 'btn btn-primary rounded-pill'; else echo 'btn btn-outline-secondary'; ?> mb-4">Pilih Sekarang</button>
                        </div>
                    </div>
                    <?php
                        }
                    }
                    ?>
                </div>

                
            </div>
        </div>

        <div class="limit pt-4 pb-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="sectionTitle text-center">
                            <h2 class="mb-3">Powerful dengan Limit PHP yang Lebih Besar</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td class="text-center"><i class="mdi mdi-check-circle float-left"></i> max execution time 300s</td>
                                </tr>
                                <tr class="table-active">
                                    <td class="text-center"><i class="mdi mdi-check-circle float-left"></i> max execution time 300s</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><i class="mdi mdi-check-circle float-left"></i> php memory limit 1024 MB</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <tr>
                                    <td class="text-center"><i class="mdi mdi-check-circle float-left"></i> post max size 128 MB</td>
                                </tr>
                                <tr class="table-active">
                                    <td class="text-center"><i class="mdi mdi-check-circle float-left"></i> upload max filesize 128 MB</td>
                                </tr>
                                <tr>
                                    <td class="text-center"><i class="mdi mdi-check-circle float-left"></i> max input vars 2500</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                    
                </div>
            </div>
        </div>

        <div class="servicearea pt-4 pb-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="sectionTitle text-center">
                            <h2 class="mb-3">Semua Paket Hosting Sudah Termasuk</h2>
                        </div>
                    </div>
                </div>
                <div class="row service">
                    <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 text-center">
                        <div class="singleService">
                            <div class="serviceIcon">
                                <img src="assets/svg/icon PHP Hosting_PHP Semua Versi.svg" alt="" width="64">
                            </div>
                            <div class="serviceContent">
                                <h3>PHP Semua Versi</h3>
                                <p>Pilih mulai dari versi 5.3 s/d PHP 7. Ubah sesuka Anda!</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 text-center">
                        <div class="singleService">
                            <div class="serviceIcon">
                                <img src="assets/svg/icon PHP Hosting_My SQL.svg" alt="" width="64">
                            </div>
                            <div class="serviceContent">
                                <h3>MySQL Versi 5.6</h3>
                                <p>Nikmati MySQL versi terbaru, tercepat dan kaya akan fitur.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 text-center">
                        <div class="singleService">
                            <div class="serviceIcon">
                                <img src="assets/svg/icon PHP Hosting_CPanel.svg" alt="" width="64">
                            </div>
                            <div class="serviceContent">
                                <h3>Panel Hosting cPanel</h3>
                                <p>Kelola website dengan cpanel canggih yang familiar di hati Anda.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 text-center">
                        <div class="singleService">
                            <div class="serviceIcon">
                                <img src="assets/svg/icon PHP Hosting_garansi uptime.svg" alt="" width="64">
                            </div>
                            <div class="serviceContent">
                                <h3>Garansi Uptime 99.9%</h3>
                                <p>Data center yang mendukung kelangsungan website Anda 24/7.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 text-center">
                        <div class="singleService">
                            <div class="serviceIcon">
                                <img src="assets/svg/icon PHP Hosting_InnoDB.svg" alt="" width="64">
                            </div>
                            <div class="serviceContent">
                                <h3>Database InnoDB Unlimited</h3>
                                <p>Jumlah dan ukuran database yang tumbuh sesuai kebutuhan Anda.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-xs-12 col-sm-12 text-center">
                        <div class="singleService">
                            <div class="serviceIcon">
                                <img src="assets/svg/icon PHP Hosting_My SQL remote.svg" alt="" width="64">
                            </div>
                            <div class="serviceContent">
                                <h3>Wildcard Remote MySQL</h3>
                                <p>Mendukung s/d 25 max_user_connections dan 100 max_connections.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="laravel pt-4 pb-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="sectionTitle text-center">
                            <h2 class="mb-3">Mendukung Penuh Framework Laravel</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
                        <h4 class="fontmontserrat">Tak perlu menggunakan dedicated server ataupun VPS yang mahal. Layanan PHP hosting murah kami mendukung penuh framework favorit Anda</h3>
                        <ul class="laravel-feature">
                            <li><i class="mdi mdi-check-circle"></i> Install Laravel <strong>1 Klik</strong> dengan Softaculous Installer.</li>
                            <li><i class="mdi mdi-check-circle"></i> Mendukung ekstensi <strong>PHP MCrypt, phar, mbstring, json, dan fileinfo.</strong></li>
                            <li><i class="mdi mdi-check-circle"></i> Tersedia <strong>Composer</strong> dan <strong>SSH</strong> untuk menginstall packages pilihan Anda.</li>
                        </ul>
                        <p class="nb">Nb. Composer dan SSH hanya tersedia pada paket Personal dan Bisnis</p>
                        <button type="button" class="btn btn-primary rounded-pill">Pilih Hosting Anda</button>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-right">
                        <div class="homeImgTable">
                            <div class="homeImg">
                                <img src="assets/svg/illustrationbannersupportlaravelhosting.svg" alt="" class="img-fluid" width="450">
                            </div>
                        </div>
                    </div>
                    
                </div>
               
            </div>
        </div>


        <div class="modul pt-4 pb-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="sectionTitle text-center">
                            <h2 class="mb-3">Modul Lengkap untuk Menjalankan Aplikasi PHP Anda.</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-3 col-lg-3 col-xs-6 col-sm-6 mb-3">
                        <ul>
                            <li>IcePHP</li>
                            <li>apc</li>
                            <li>apcu</li>
                            <li>apm</li>
                            <li>ares</li>
                            <li>bcmath</li>
                            <li>bcompiler</li>
                            <li>big_int</li>
                            <li>bitset</li>
                            <li>bloomy</li>
                            <li>bz2_filter</li>
                            <li>clamav</li>
                            <li>coin_acceptor</li>
                            <li>crack</li>
                            <li>dba</li>
                        </ul>
                    </div>
                    <div class="col col-md-3 col-lg-3 col-xs-6 col-sm-6 mb-3">
                        <ul>
                            <li>http</li>
                            <li>huffman</li>
                            <li>idn</li>
                            <li>igbinary</li>
                            <li>imagick</li>
                            <li>imap</li>
                            <li>inclued</li>
                            <li>inotify</li>
                            <li>interbase</li>
                            <li>intl</li>
                            <li>ioncube_loader</li>
                            <li>ioncube_loader_4</li>
                            <li>jsmin</li>
                            <li>json</li>
                            <li>idap</li>
                        </ul>
                    </div>
                    <div class="col col-md-3 col-lg-3 col-xs-6 col-sm-6 mb-3">
                        <ul>
                            <li>nd_pdo_mysql</li>
                            <li>oauth</li>
                            <li>oci8</li>
                            <li>odbc</li>
                            <li>opcache</li>
                            <li>pdf</li>
                            <li>pdo</li>
                            <li>pdo_dblib</li>
                            <li>interbase</li>
                            <li>intl</li>
                            <li>ioncube_loader</li>
                            <li>ioncube_loader_4</li>
                            <li>jsmin</li>
                            <li>json</li>
                            <li>idap</li>
                        </ul>
                    </div>
                    <div class="col col-md-3 col-lg-3 col-xs-6 col-sm-6 mb-3">
                        <ul>
                            <li>nd_pdo_mysql</li>
                            <li>oauth</li>
                            <li>oci8</li>
                            <li>odbc</li>
                            <li>opcache</li>
                            <li>pdf</li>
                            <li>pdo</li>
                            <li>pdo_dblib</li>
                            <li>interbase</li>
                            <li>intl</li>
                            <li>ioncube_loader</li>
                            <li>ioncube_loader_4</li>
                            <li>jsmin</li>
                            <li>json</li>
                            <li>idap</li>
                        </ul>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-12 text-center">
                        <button class="btn btn-outline-secondary">Selengkapnya</button>
                    </div>
                </div>
               
            </div>
        </div>

        <div class="linux pt-4 pb-4">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="sectionTitle">
                            <h2 class="mb-3">Linux Hosting yang Stabil dengan Teknologi LVE</h2>
                        </div>
                        <p class="mb-4">SuperMicro <strong>Intel Xeon 24-Cores</strong> server dengan RAM <strong>128 GB</strong>
                        dan teknologi LIVE CloudLinux untuk stabilitas server Anda. Dilengkapi dengan SSD untuk kecepatan MySQL dan caching, Apache
                    load balancer berbasis LiteSpeed Technologies, CageFS security, Raid-10 protection dan auto backup
                untuk keamanan website PHP Anda.</p>
                <button type="button" class="btn btn-primary rounded-pill">Pilih Hosting Anda</button>
                    </div>
                    <div class="col-lg-5">
                        <img src="assets/images/Image support.png" alt="" class="img-fluid" width="450">
                    </div>
                </div>
            </div>
        </div>

        
        <div class="sectionshare">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7">
                        <p>Bagikan jika Anda menyukai halaman ini.</p>
                    </div>
                    <div class="col-lg-5 text-right">
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="sectionbantuan">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-lg-9 col-md-9 support">
                        <p>Perlu <strong>BANTUAN?</strong> Hubungi Kami: <strong>0274-5305505</strong></p>
                    </div>
                    <div class="col-xs-12 col-lg-3 col-md-3 supportbutton">
                        <button class="btn btn-outline-white"><i class="mdi mdi-message"></i> Live Chat</button>
                    </div>
                </div>
            </div>
        </div>

        <footer>
            <div class="container">
                <div class="row mb-5">
                    <div class="col-6 col-lg-3 col-md-3">
                        <div class="widget">
                            <div class="h4">HUBUNGI KAMI</div>
                            <ul class="footerLink">
                                <li><a href="#">0274-5305505</a></li>
                                <li><a href="#">Senin-Minggu</a></li>
                                <li><a href="#">24 Jam Nonstop</a></li>
                            </ul>
                            <p class="mt-3">Jl. Selokan Mataram Monjali<br>Karangjati MT I/304<br>Sinduadi, Mlati,Sleman<br>Yogyakarta 55284</p>
                        </div>
                    </div>
                    <div class="col-6 col-lg-3 col-md-3">
                        <div class="widget">
                            <div class="h4">LAYANAN</div>
                            <ul class="footerLink">
                                <li><a href="#">Domain</a></li>
                                <li><a href="#">Shared Hosting</a></li>
                                <li><a href="#">Cloud VPS Hosting</a></li>
                                <li><a href="#">Managed VPS Hosting</a></li>
                                <li><a href="#">Web Builder</a></li>
                                <li><a href="#">Keamanan SSL/HTTPS</a></li>
                                <li><a href="#">Jasa Pembuatan Website</a></li>
                                <li><a href="#">Program Affiliasi</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-6 col-lg-3 col-md-3">
                        <div class="widget">
                            <div class="h4">SERVICE HOSTING</div>
                            <ul class="footerLink">
                                <li><a href="#">Hosting Murah</a></li>
                                <li><a href="#">Hosting Indonesia</a></li>
                                <li><a href="#">Hosting Singapura SG</a></li>
                                <li><a href="#">Hosting PHP</a></li>
                                <li><a href="#">Hosting Wordpress</a></li>
                                <li><a href="#">Hosting Laravel</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-6 col-lg-3 col-md-3">
                        <div class="widget">
                            <div class="h4">TUTORIAL</div>
                            <ul class="footerLink">
                                <li><a href="#">Knowledgebase</a></li>
                                <li><a href="#">Blog</a></li>
                                <li><a href="#">Cara Pembayaran</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-6 col-lg-3 col-md-3">
                        <div class="widget">
                            <div class="h4">TENTANG KAMI</div>
                            <ul class="footerLink">
                                <li><a href="#">Tim Niagahoster</a></li>
                                <li><a href="#">Karir</a></li>
                                <li><a href="#">Events</a></li>
                                <li><a href="#">Penawaran & Promo Spesial</a></li>
                                <li><a href="#">Kontak Kami</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-6 col-lg-3 col-md-3">
                        <div class="widget">
                            <div class="h4">KENAPA PILIH NIAGAHOSTER?</div>
                            <ul class="footerLink">
                                <li><a href="#">Support Terbaik</a></li>
                                <li><a href="#">Garansi Harga Termurah</a></li>
                                <li><a href="#">Domain Gratis Selamanya</a></li>
                                <li><a href="#">Datacenter Hosting Terbaik</a></li>
                                <li><a href="#">Review Pelanggan</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="widget">
                            <div class="h4">NEWSLETTER</div>
                            <div class="input-group mb-2">
                            <input type="text" class="form-control" placeholder="Email">
                            <div class="input-group-append">
                                <button type="button">Berlangganan</button>
                            </div>
                            </div>
                            <p>Dapatkan promo dan konten menarik dari penyedia hosting terbaik Anda.</p>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <a href="" class="footericon"><i class="mdi mdi-facebook"></i></a>
                        <a href="" class="footericon"><i class="mdi mdi-twitter"></i></a>
                        <a href="" class="footericon"><i class="mdi mdi-google-plus"></i></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="widget">
                            <div class="h4">PEMBAYARAN</div>
                            <ul class="logobank">
                                <li><img src="assets/images/logo-bca-bordered.svg"></li>
                                <li><img src="assets/images/mandiriclickpay.svg"></li>
                                <li><img src="assets/images/logo-bni-bordered.svg"></li>
                                <li><img src="assets/images/visa.svg"></li>
                                <li><img src="assets/images/mastercard.svg"></li>
                                <li><img src="assets/images/atmbersama.svg"></li>
                                <li><img src="assets/images/permatabank.svg"></li>
                                <li><img src="assets/images/prima.svg"></li>
                                <li><img src="assets/images/alto.svg"></li>
                            </ul>
                        </div>
                        <p>Aktivasi instan dengan e-Payment Hosting dan domain langsung aktif</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <hr class="super-soft">
                    </div>
                    <div class="col-lg-8">
                        <p class="copyright">Copyright &copy; 2016 Niagahoster |  Hosting powered by PHP7, CloudLinux, CloudFlare, BitNinja and DC Biznet Technovillage Jakarta<br>
                    Cloud VPS Murah powered by Webuzo Softaculous, Intel SSD and cloud computing technology</p>
                    </div>
                    <div class="col-lg-4 text-right">
                        <p class="copyright">Syarat dan Ketentuan | Kebijakan Privasi</p>
                    </div>
                </div>

                
            </div>
        </footer>

        

        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/scripts.js"></script>
    </body>
</html>