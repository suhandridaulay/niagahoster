<?php


class Package
{
    private $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    public function getAll()
    {
        $sql = "SELECT * FROM package ORDER BY packagePrice ASC";
        return $this->db->executeQuery($sql);
    }
}